const mongoose = require('mongoose');
const assert = require('assert');
const { registerProspectData } = require('../src/services/default.service');

describe('Backend Test', async function () {
	this.timeout(60000);
	before(async () => {
		await mongoose
			.connect('mongodb+srv://ano:mdp2merde@mspr-dev-appli-b3.s6wxi.mongodb.net/test')
			.then(() => console.log('*** connected to MongoDB'))
			.catch((err) => {
				console.log(err);
				throw err;
			});
	});

	describe('Default test', async function () {
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
		it('Just a default test', async function () {
			assert.deepStrictEqual(true, true);
		});
	});

	describe('Prospect register test', async function () {
		it('Result shall not be null', async function () {
			const result = await registerProspectData('arnaud', 'arnaud.gonin@live.fr');
			assert.deepStrictEqual(result.firstName, 'arnaud');
		});
	});
});
