const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3000;

mongoDbConnect()
	.then(() => console.log('[***] mongoDB connected'))
	.catch((err) => console.log(err));

app.use(bodyParser.json());

app.use('', require('./src/routes/default.route'));

app.listen(port, () => {
	console.log(`[***] app running on port http://localhost:${port}`);
});

async function mongoDbConnect() {
	await mongoose.connect('mongodb+srv://ano:mdp2merde@mspr-dev-appli-b3.s6wxi.mongodb.net/test');
}
