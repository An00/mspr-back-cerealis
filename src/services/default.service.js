const customerModel = require('../models/default.model');

async function registerProspectData(firstName, email) {
	const result = await customerModel
		.findOneAndUpdate({ email: email }, { firstName: firstName }, { upsert: true })
		.exec();
	return result;
}

module.exports = { registerProspectData };
