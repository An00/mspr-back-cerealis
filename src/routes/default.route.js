const express = require('express');
const { registerProspectData } = require('../services/default.service');

const router = express.Router();

router.get('/', (req, res) => {
	res.send('hello world');
});

router.post('/prospect', async (req, res) => {
	const { firstName, email, options } = req.body || {};
	if (!firstName || typeof firstName !== 'string') {
		return res.status(404).send('firstName required');
	}

	if (!email || typeof email !== 'string') {
		return res.status(404).send('email required');
	}

	try {
		const result = await registerProspectData(firstName, email);
		if (result) {
			return res.status(200).send(result);
		}

		return res.status(400).send({ sucess: false, message: result });
	} catch (error) {
		return res.status(500).send({ sucess: false, message: error });
	}
});

module.exports = router;
