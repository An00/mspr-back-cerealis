const mongoose = require('mongoose');

const customerSchema = mongoose.Schema(
	{
		firstName: {
			type: 'String',
			required: false,
			default: 'unknown',
		},

		email: {
			type: 'String',
			required: false,
			default: 'unknown',
		},
	},
	{
		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
	}
);

// Register it to MongoDB
const customerModel = mongoose.model('customer', customerSchema);

module.exports = customerModel;
